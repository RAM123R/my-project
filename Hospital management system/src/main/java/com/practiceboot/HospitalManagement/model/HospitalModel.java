package com.practiceboot.HospitalManagement.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name="Hospital_details")
public class HospitalModel {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)

private int patient_id;
private String name;
private int age;
private String address;
private long moblileNumber;
public int getPatient_id() {
	return patient_id;
}
public void setPatient_id(int patient_id) {
	this.patient_id = patient_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public long getMoblileNumber() {
	return moblileNumber;
}
public void setMoblileNumber(long moblileNumber) {
	this.moblileNumber = moblileNumber;
}




}
