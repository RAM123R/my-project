package com.practiceboot.HospitalManagement.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.practiceboot.HospitalManagement.model.HospitalModel;
import com.practiceboot.HospitalManagement.repository.HospitalRepo;

@Controller
public class HospitalController {
	@Autowired
	HospitalRepo repo;
	
	@RequestMapping("index")
	public ModelAndView viewPat() {
		ModelAndView mav=new ModelAndView("listPatient");
		mav.addObject("patientObj",repo.findAll());
		return mav;
	}
	@RequestMapping("patient/new")
	public ModelAndView  createPatient(HospitalModel pat) {

		ModelAndView mav = new ModelAndView("createPatient");																												
		mav.addObject("Obj",pat);
		return mav; 
	}  
	@PostMapping("patient/new")
	public String savePatient(HospitalModel pat) {
		
		repo.save(pat);
	return "redirect:/index";
	}


	@RequestMapping("patient/edit/{id}")
public ModelAndView editpatient(@PathVariable int id) {
		
	ModelAndView mav = new ModelAndView("updatepatient");
		mav.addObject("Obj",repo.findById(id).get());
		return mav;
}
	@PostMapping("patient/edit")
	public String updatePatient(HospitalModel model) {		
		repo.save(model);
		return "redirect:/index";
	}
	
	@RequestMapping("patient/delete/{id}")
	public String deletePatient(@PathVariable int id) {
		repo.deleteById(id);
		return "redirect:/index";
	}
	

	
}
