package com.practiceboot.HospitalManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practiceboot.HospitalManagement.model.HospitalModel;

public interface HospitalRepo extends JpaRepository <HospitalModel,Integer>{

}
